from time import sleep
import requests
from pprint import pprint
from functools import wraps


def print_function_name(func):
    @wraps(func)
    def wrapped(*args, **kwargs):
        print(f'\n# {func.__name__}')
        return func(*args, **kwargs)
    return wrapped


@print_function_name
def get_info():
    response = requests.get(f'{protocol}://{mDNS}/json/info')
    info = response.json()
    pprint(info)


@print_function_name
def get_state():
    response = requests.get(f'{protocol}://{board_ip}/json/state')
    state = response.json()
    pprint(state)


@print_function_name
def get_palettes():
    response = requests.get(f'{protocol}://{board_ip}/json/palettes')
    palettes = response.json()
    pprint(palettes)


@print_function_name
def get_effects():
    response = requests.get(f'{protocol}://{board_ip}/json/effects')
    effects = response.json()
    pprint(effects)


@print_function_name
def turn_on():
    body = {"on": True, "bri": 50}
    headers = {'Content-Type': 'application/json'}
    response = requests.post(f'{protocol}://{board_ip}/json/state', json=body, headers=headers)
    json_data = response.json()
    print(json_data)


@print_function_name
def turn_off():
    body = {"on": False}
    headers = {'Content-Type': 'application/json'}
    response = requests.post(f'{protocol}://{board_ip}/json/state', json=body, headers=headers)
    json_data = response.json()
    print(json_data)


@print_function_name
def get_all():
    get_info()
    get_state()
    get_palettes()
    get_effects()
    turn_off()
    sleep(3)
    turn_on()


if __name__ == "__main__":
    board_ip = '192.168.1.249'
    mDNS = 'wled-jt1.local'
    protocol = 'http'
    get_all()
