import requests
from pprint import pprint
from time import sleep


class WLED:
    ip = None
    mDNS = ''
    protocol = 'http'
    headers = {'Content-Type': 'application/json'}

    def __init__(self, mDNS, ip=None):
        self.mDNS = mDNS
        if not ip:
            self.ip = self._find_ip()
        else:
            self.ip = ip

    def _find_ip(self):
        ip = None
        try:
            info_response = self.get('info')
            if info_response.status_code == 200:
                info_dict = info_response.json()
                ip = info_dict['ip']
        except Exception as error:
            print('Count not retrieve IP Address, is it online? is the mDNS name correct?')
            print(error)
        return ip

    def _url_for(self, target_object):
        host = self.ip or self.mDNS
        return f'{self.protocol}://{host}/json/{target_object}'

    def get(self, target_object):
        return requests.get(self._url_for(target_object))

    def post(self, target_object, json):
        return requests.post(self._url_for(target_object), json=json, headers=self.headers)


def example():
    light_strip = WLED('wled-jt1.local')

    # GET STATE
    print('\n# GET STATE #')
    status_response = light_strip.get('state')
    pprint(status_response.json())

    # TURN OFF
    print('\n# TURN OFF #')
    off_response = light_strip.post('state', {'on': False})
    pprint(off_response.json())

    sleep(3)

    # TURN ON
    print('\n# TURN ON #')
    on_response = light_strip.post('state', {'on': True, 'bri': 128})
    pprint(on_response.json())


if __name__ == '__main__':
    example()
