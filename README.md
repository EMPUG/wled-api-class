# WLED Convenience Class

A class to perform WLED requests conveniently.

* Uses `requests` so if you are using circuit python this should just work, maybe.
* Will accept `mDNS` name and or `ip`, will try to resolve the IP address and prefer that for performance. 
I.E. You can initialize an object with just the mDNS name, it will only do a DNS query once and then use the IP address. 
You can also supply the `ip` upfront if you want to avoid the initial delay caused by the DNS query.
* The local functions file is essentially just a few example functions without the class.

I might work on expanding this later, it is currently not designed to be helpful in debugging, so if you are having trouble try implementing the local functions as a simple test